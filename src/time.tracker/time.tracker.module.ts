import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TimeRangeEntity } from './time.range/time.range.entity';
import { TimeRangeRequestService } from './time.range/time.range.request.service';
import { TimeRangeController } from './time.range/time.range.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([TimeRangeEntity]),
  ],
  providers: [
    TimeRangeRequestService,
  ],
  controllers: [
    TimeRangeController,
  ],
})
export class TimeTrackerModule {
}