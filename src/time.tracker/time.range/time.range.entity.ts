import { Column, Entity, Index, ManyToOne } from 'typeorm';
import { UserEntity } from '../../users/users/user.entity';
import { BaseEntity } from '../../common/typeorm/base.entity';

@Entity({ name: 'time_ranges' })
export class TimeRangeEntity extends BaseEntity {
  @Column()
  name: string;

  @Column()
  @Index()
  startedAt: Date;

  @Column()
  finishedAt: Date;

  @ManyToOne(
    () => UserEntity,
    (user) => user.timeRanges,
  )
  user: UserEntity;

  constructor(data: Partial<TimeRangeEntity>) {
    super();
    Object.assign(this, data);
  }
}