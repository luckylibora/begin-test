import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { QueryFailedError, Repository } from 'typeorm';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { TimeRangeRequestService } from './time.range.request.service';
import { TimeRangeEntity } from './time.range.entity';
import { ContextIdFactory, REQUEST } from '@nestjs/core';
import { UserEntity } from '../../users/users/user.entity';

describe('TimeRangeRequestService', () => {
  let timeRangeRequestService: TimeRangeRequestService;
  let repository: Repository<TimeRangeEntity>;

  const repositoryToken = getRepositoryToken(TimeRangeEntity);
  const timeRangeRepositoryMock = {
    insert: jest.fn(),
    findAndCount: jest.fn(),
    findOne: jest.fn(),
    save: jest.fn(),
  };
  const requestUser = new UserEntity({
    id: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
    email: 'test@test.com',
    passwordHash: '123rfafa',
    passwordSalt: 'r23f32f',
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TimeRangeRequestService,
        {
          provide: repositoryToken,
          useValue: timeRangeRepositoryMock,
        },
        {
          provide: REQUEST,
          useValue: {
            user: requestUser,
          },
        },
      ],
    }).compile();

    const contextId = ContextIdFactory.create();
    jest.spyOn(ContextIdFactory, 'getByRequest')
      .mockImplementation(() => contextId);
    timeRangeRequestService = await module.resolve(TimeRangeRequestService, contextId);
    repository = module.get(repositoryToken);
  });

  describe('#create', () => {
    it('should create time range', async () => {
      const mockedInsert = jest.spyOn(repository, 'insert');
      await timeRangeRequestService.create({ name: 'test' });
      const insertArgs = mockedInsert.mock.calls[0];
      const timeRangeEntityArg = insertArgs[0];
      expect(timeRangeEntityArg).toHaveProperty('name', 'test');
      expect(timeRangeEntityArg).toHaveProperty('user', requestUser);
    });

    it('should throw BadRequestException if user with email is already created', async () => {
      const queryErr = new QueryFailedError(
        '',
        [],
        'SQLITE_CONSTRAINT: UNIQUE constraint failed: time_ranges.name, time_ranges.userId',
      );
      jest.spyOn(repository, 'insert').mockRejectedValue(queryErr);
      const createDto = { name: 'test' };
      try {
        await timeRangeRequestService.create(createDto);
        fail('exception is not thrown');
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
      }
    });

    it('should throw original error in other cases', async () => {
      const originalErr = new Error();
      jest.spyOn(repository, 'insert').mockRejectedValue(originalErr);
      const createDto = { name: 'test' };
      try {
        await timeRangeRequestService.create(createDto);
        fail('exception is not thrown');
      } catch (err) {
        expect(err).toBe(originalErr);
      }
    });
  });

  describe('#finish', () => {
    it('should return entity with finishedAt prop', async () => {
      const savedTimeRange = new TimeRangeEntity({
        id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
        name: 'test',
        user: requestUser,
        startedAt: new Date(),
        finishedAt: null,
      });
      jest.spyOn(repository, 'findOne')
        .mockResolvedValue(savedTimeRange);
      jest.spyOn(repository, 'save').mockResolvedValue(null);
      const updatedEntity = await timeRangeRequestService.finish(1);
      expect(updatedEntity.finishedAt).not.toBeNull();
    });

    it('should throw NotFoundException if time range is not exist or was created by other user', async () => {
      jest.spyOn(repository, 'findOne')
        .mockResolvedValue(null);
      try {
        await timeRangeRequestService.finish(1);
        fail(new Error('should throw exception'));
      } catch (err) {
        expect(err).toBeInstanceOf(NotFoundException);
      }
    });

    it('should throw BadRequestException if time range is already finished', async () => {
      const savedTimeRange = new TimeRangeEntity({
        id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
        name: 'test',
        user: requestUser,
        startedAt: new Date(),
        finishedAt: new Date(),
      });
      jest.spyOn(repository, 'findOne')
        .mockResolvedValue(savedTimeRange);
      try {
        await timeRangeRequestService.finish(1);
        fail(new Error('should throw exception'));
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
      }
    });

    it('should throw BadRequestException if time range is not started', async () => {
      const savedTimeRange = new TimeRangeEntity({
        id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
        name: 'test',
        user: requestUser,
        startedAt: null,
        finishedAt: null,
      });
      jest.spyOn(repository, 'findOne')
        .mockResolvedValue(savedTimeRange);
      try {
        await timeRangeRequestService.finish(1);
        fail(new Error('should throw exception'));
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
      }
    });
  });

  describe('#list', () => {
    it('should return list of time ranges', async () => {
      const savedEntities = [
        new TimeRangeEntity({
          id: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
          name: 'test1',
          user: requestUser,
          startedAt: new Date(),
          finishedAt: null,
        }),
        new TimeRangeEntity({
          id: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
          name: 'test2',
          user: requestUser,
          startedAt: new Date(),
          finishedAt: null,
        }),
      ];
      jest.spyOn(repository, 'findAndCount')
        .mockResolvedValue([savedEntities, 2]);
      const list = await timeRangeRequestService.list({ limit: 10, skip: 0 });
      expect(list.entities).toBe(savedEntities);
      expect(list.count).toBe(2);
    });

    it('should should add like filter if q parameter passed', async () => {
      const spyOnFindAndCount = jest.spyOn(repository, 'findAndCount')
        .mockClear()
        .mockResolvedValue([[], 0]);
      await timeRangeRequestService.list({ limit: 100, skip: 0, q: 'test' });
      const args = spyOnFindAndCount.mock.calls[0];
      const opts = args[0];
      expect(opts).toHaveProperty('where.name');
    });
  });

  describe('#start', () => {
    it('should return entity with startedAt prop', async () => {
      const savedTimeRange = new TimeRangeEntity({
        id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
        name: 'test',
        user: requestUser,
        startedAt: null,
        finishedAt: null,
      });
      jest.spyOn(repository, 'findOne')
        .mockResolvedValue(savedTimeRange);
      jest.spyOn(repository, 'save').mockResolvedValue(null);
      const updatedEntity = await timeRangeRequestService.start(1);
      expect(updatedEntity.startedAt).not.toBeNull();
    });

    it('should throw NotFoundException if time range is not exist or was created by other user', async () => {
      jest.spyOn(repository, 'findOne')
        .mockResolvedValue(null);
      try {
        await timeRangeRequestService.start(1);
        fail(new Error('should throw exception'));
      } catch (err) {
        expect(err).toBeInstanceOf(NotFoundException);
      }
    });

    it('should throw BadRequestException if time range is already started', async () => {
      const savedTimeRange = new TimeRangeEntity({
        id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
        name: 'test',
        user: requestUser,
        startedAt: new Date(),
        finishedAt: null,
      });
      jest.spyOn(repository, 'findOne')
        .mockResolvedValue(savedTimeRange);
      try {
        await timeRangeRequestService.start(1);
        fail(new Error('should throw exception'));
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
      }
    });
  });

  describe('#updateByid', () => {
    it('should return updated entity', async () => {
      const savedTimeRange = new TimeRangeEntity({
        id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
        name: 'test',
        user: requestUser,
        startedAt: new Date(),
        finishedAt: null,
      });
      jest.spyOn(repository, 'save')
        .mockResolvedValue(savedTimeRange);
      jest.spyOn(repository, 'findOne')
        .mockResolvedValue(savedTimeRange);
      const updatedEntity = await timeRangeRequestService.updateById(1, { name: 'test2' });
      expect(updatedEntity.name).toBe('test2');
    });

    it('should throw NotFoundException', async () => {
      jest.spyOn(repository, 'findOne').mockResolvedValue(null);
      try {
        await timeRangeRequestService.updateById(1, { name: 'test2' });
        fail(new Error('should throw error'));
      } catch (err) {
        expect(err).toBeInstanceOf(NotFoundException);
      }
    });

    it('should throw BadRequestException for name which is already presented', async () => {
      const queryErr = new QueryFailedError(
        '',
        [],
        'SQLITE_CONSTRAINT: UNIQUE constraint failed: time_ranges.name, time_ranges.userId',
      );
      const savedTimeRange = new TimeRangeEntity({
        id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
        name: 'test',
        user: requestUser,
        startedAt: new Date(),
        finishedAt: null,
      });
      jest.spyOn(repository, 'save').mockRejectedValue(queryErr);
      jest.spyOn(repository, 'findOne')
        .mockResolvedValue(savedTimeRange);
      try {
        await timeRangeRequestService.updateById(1, { name: 'test2' });
        fail('exception is not thrown');
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
      }
    });
  });
});