import { Body, Controller, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../../auth/jwt/jwt.auth.guard';
import { TimeRangeCreateDto } from './dto/time.range.create.dto';
import { TimeRangeRequestService } from './time.range.request.service';
import { TimeRangeResponse } from './dto/time.range.response';
import { plainToClass } from 'class-transformer';
import { TimeRangeListDto } from './dto/time.range.list.dto';
import { TimeRangeListResponse } from './dto/time.range.list.response';
import { IdParamDto } from '../../common/dto/id.param.dto';
import { TimeRangeUpdateDto } from './dto/time.range.update.dto';
import { ApiBearerAuth, ApiOkResponse } from '@nestjs/swagger';

@Controller('time-ranges')
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
export class TimeRangeController {
  constructor(
    private timeRangeService: TimeRangeRequestService,
  ) {
  }

  @Post()
  @ApiOkResponse({type: TimeRangeResponse})
  async create(@Body() dto: TimeRangeCreateDto): Promise<TimeRangeResponse> {
    const entity = await this.timeRangeService.create(dto);
    return plainToClass(TimeRangeResponse, entity);
  }

  @Put('/:id/finish')
  @ApiOkResponse({type: TimeRangeResponse})
  async finish(@Param() { id }: IdParamDto): Promise<TimeRangeResponse> {
    const entity = await this.timeRangeService.finish(id);
    return plainToClass(TimeRangeResponse, entity);
  }

  @Get()
  @ApiOkResponse({type: TimeRangeListResponse})
  async list(@Query() dto: TimeRangeListDto): Promise<TimeRangeListResponse> {
    const entityList = await this.timeRangeService.list(dto);
    return plainToClass(TimeRangeListResponse, entityList);
  }

  @Put('/:id/start')
  @ApiOkResponse({type: TimeRangeResponse})
  async start(@Param() { id }: IdParamDto): Promise<TimeRangeResponse> {
    const entity = await this.timeRangeService.start(id);
    return plainToClass(TimeRangeResponse, entity);
  }

  @Put('/:id')
  @ApiOkResponse({type: TimeRangeResponse})
  async update(@Param() { id }: IdParamDto, @Body() body: TimeRangeUpdateDto): Promise<TimeRangeResponse> {
    const entity = await this.timeRangeService.updateById(id, body);
    return plainToClass(TimeRangeResponse, entity);
  }
}