import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { ExecutionContext, INestApplication, ValidationPipe } from '@nestjs/common';
import { TimeRangeRequestService } from './time.range.request.service';
import { TimeRangeController } from './time.range.controller';
import { JwtAuthGuard } from '../../auth/jwt/jwt.auth.guard';
import { TimeRangeEntity } from './time.range.entity';
import { UserEntity } from '../../users/users/user.entity';

describe('TimeRangeController', () => {
  let app: INestApplication;
  let timeRangeRequestService: TimeRangeRequestService;

  const requestUser = new UserEntity({
    id: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
    email: 'test@test.com',
    passwordHash: '123rfafa',
    passwordSalt: 'r23f32f',
  });
  const jwtAuthGuardMock = {
    canActivate: (context: ExecutionContext) => {
      const request = context.switchToHttp().getRequest();
      request.user = requestUser;
      return true;
    },
  };
  const timeRangeRequestServiceMock = {
    create: jest.fn(),
    finish: jest.fn(),
    list: jest.fn(),
    start: jest.fn(),
    updateById: jest.fn(),
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        TimeRangeRequestService,
      ],
      controllers: [TimeRangeController],
    })
      .overrideGuard(JwtAuthGuard)
      .useValue(jwtAuthGuardMock)
      .overrideProvider(TimeRangeRequestService)
      .useValue(timeRangeRequestServiceMock)
      .compile();

    app = moduleRef.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
    timeRangeRequestService = moduleRef.get(TimeRangeRequestService);
  });

  describe('POST /time-ranges', () => {
    it('should create new user', () => {
      jest.spyOn(timeRangeRequestService, 'create')
        .mockResolvedValue(new TimeRangeEntity({
          id: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
          name: 'test',
          startedAt: null,
          finishedAt: null,
        }));
      return request(app.getHttpServer())
        .post('/time-ranges')
        .send({ name: 'test' })
        .expect(201);
    });
  });

  describe('PUT /time-ranges/:id/start', () => {
    it('should return updated time range', async () => {
      jest.spyOn(timeRangeRequestService, 'start')
        .mockResolvedValue(new TimeRangeEntity({
          id: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
          name: 'test',
          startedAt: new Date(),
          finishedAt: null,
        }));
      return request(app.getHttpServer())
        .put('/time-ranges/1/start')
        .expect(200);
    });
  });

  describe('PUT /time-ranges/:id/finish', () => {
    it('should return updated time range', async () => {
      jest.spyOn(timeRangeRequestService, 'finish')
        .mockResolvedValue(new TimeRangeEntity({
          id: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
          name: 'test',
          startedAt: new Date(),
          finishedAt: new Date(),
        }));
      return request(app.getHttpServer())
        .put('/time-ranges/1/finish')
        .expect(200);
    });
  });

  describe('GET /time-ranges', () => {
    it('should return list of entities', async () => {
      const savedEntities = [
        new TimeRangeEntity({
          id: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
          name: 'test1',
          user: requestUser,
          startedAt: new Date(),
          finishedAt: null,
        }),
        new TimeRangeEntity({
          id: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
          name: 'test2',
          user: requestUser,
          startedAt: new Date(),
          finishedAt: null,
        }),
      ];

      jest.spyOn(timeRangeRequestService, 'list')
        .mockResolvedValue({ entities: savedEntities, count: 2 });
      return request(app.getHttpServer())
        .get('/time-ranges?limit=10&skip=0')
        .expect(200)
        .expect((res) => {
          expect(res.body.count).toBe(2);
          expect(res.body.entities).toHaveLength(2);
        });
    });

    it('should return 400 for limit more than 100', async () => {
      return request(app.getHttpServer())
        .get('/time-ranges?limit=101&skip=0')
        .expect(400);
    });

    it('should return 400 if q parameter q is less than 3 character long', async () => {
      return request(app.getHttpServer())
        .get('/time-ranges?limit=20&skip=0&q=te')
        .expect(400);
    });
  });

  describe('PUT /time-ranges/:id', () => {
    it('should return updated time range', async () => {
      jest.spyOn(timeRangeRequestService, 'updateById')
        .mockResolvedValue(new TimeRangeEntity({
          id: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
          name: 'test1',
          user: requestUser,
          startedAt: new Date(),
          finishedAt: null,
        }));
      return request(app.getHttpServer())
        .put('/time-ranges/1')
        .send({ name: 'test2' })
        .expect(200);
    });
  });

  afterAll(async () => {
    await app.close();
  });
});