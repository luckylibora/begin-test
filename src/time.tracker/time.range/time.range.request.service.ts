import { BadRequestException, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TimeRangeEntity } from './time.range.entity';
import { FindConditions, QueryFailedError, Repository, Like } from 'typeorm';
import { TimeRangeCreateDto } from './dto/time.range.create.dto';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { UserEntity } from '../../users/users/user.entity';
import { TimeRangeListDto } from './dto/time.range.list.dto';
import { EntityList } from '../../common/typeorm/entity.list';
import { TimeRangeUpdateDto } from './dto/time.range.update.dto';

@Injectable()
export class TimeRangeRequestService {
  private get user(): UserEntity {
    return this.request.user as UserEntity;
  }

  constructor(
    @Inject(REQUEST)
    private request: Request,
    @InjectRepository(TimeRangeEntity)
    private timeRangeRepository: Repository<TimeRangeEntity>,
  ) {
  }

  async create({ name }: TimeRangeCreateDto): Promise<TimeRangeEntity> {
    try {
      const timeRangeEntity = new TimeRangeEntity({
        name,
        user: this.user,
      });
      await this.timeRangeRepository.insert(timeRangeEntity);
      return timeRangeEntity;
    } catch (err) {
      this.handleDbWriteError(err);
    }
  }

  async findById(id: number): Promise<TimeRangeEntity> {
    return this.timeRangeRepository.findOne({
      where: {
        id,
        user: this.user,
      },
    });
  }

  async finish(id: number): Promise<TimeRangeEntity> {
    const entity = await this.findById(id);
    if (!entity) {
      throw new NotFoundException();
    }

    if (entity.finishedAt) {
      throw new BadRequestException('Time range is already started');
    }

    if (!entity.startedAt) {
      throw new BadRequestException('Time range is not started');
    }

    entity.finishedAt = new Date();
    await this.timeRangeRepository.save(entity);
    return entity;
  }

  async list({ limit, skip, q }: TimeRangeListDto): Promise<EntityList<TimeRangeEntity>> {
    const where: FindConditions<TimeRangeEntity> = {
      user: this.user,
    };
    if (q) {
      where.name = Like(`%${q}%`);
    }

    const [entities, count] = await this.timeRangeRepository.findAndCount({
      where,
      skip: skip,
      take: limit,
      order: {
        startedAt: -1,
      },
    });
    return { entities, count };
  }

  async start(id: number): Promise<TimeRangeEntity> {
    const entity = await this.findById(id);
    if (!entity) {
      throw new NotFoundException();
    }

    if (entity.startedAt) {
      throw new BadRequestException('Time range is already started');
    }

    entity.startedAt = new Date();
    await this.timeRangeRepository.save(entity);
    return entity;
  }

  async updateById(id: number, { name }: TimeRangeUpdateDto): Promise<TimeRangeEntity> {
    const entity = await this.findById(id);
    if (!entity) {
      throw new NotFoundException();
    }

    try {
      entity.name = name;
      await this.timeRangeRepository.save(entity);
      return entity;
    } catch (err) {
      this.handleDbWriteError(err);
    }
  }

  private handleDbWriteError(err: Error) {
    if (err instanceof QueryFailedError) {
      if (err.message === 'SQLITE_CONSTRAINT: UNIQUE constraint failed: time_ranges.name, time_ranges.userId') {
        throw new BadRequestException(`Time range with this name is already created`);
      }
    }
    throw err;
  }
}