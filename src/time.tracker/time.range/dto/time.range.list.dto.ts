import { IsInt, Max, Min, MinLength, ValidateIf } from 'class-validator';
import { Type } from 'class-transformer';
import { maxLimitValue, minSearchQueryLength } from '../../../common/dto/validation.consts';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class TimeRangeListDto {
  @IsInt()
  @Min(0)
  @Max(maxLimitValue)
  @Type(() => Number)
  @ApiModelProperty()
  limit: number;

  @IsInt()
  @Min(0)
  @Type(() => Number)
  @ApiModelProperty()
  skip: number;

  @ValidateIf((dto) => dto.q)
  @MinLength(minSearchQueryLength)
  @ApiModelProperty({ required: false })
  q?: string;
}