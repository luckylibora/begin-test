import { TimeRangeEntity } from '../time.range.entity';
import { TimeRangeResponse } from './time.range.response';
import { Type } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class TimeRangeListResponse {
  @ApiModelProperty()
  count: number;

  @Type(() => TimeRangeResponse)
  @ApiModelProperty({
    isArray: true,
    type: TimeRangeResponse,
  })
  entities: TimeRangeEntity;
}