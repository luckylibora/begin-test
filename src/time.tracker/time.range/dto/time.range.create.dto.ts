import { IsDefined } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class TimeRangeCreateDto {
  @IsDefined()
  @ApiModelProperty()
  name: string;
}