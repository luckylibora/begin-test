import { BaseResponse } from '../../../common/dto/base.response';
import { Exclude, Expose } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

@Exclude()
export class TimeRangeResponse extends BaseResponse {
  @Expose()
  @ApiModelProperty()
  name: string;

  @Expose()
  @ApiModelProperty()
  startedAt: Date;

  @Expose()
  @ApiModelProperty()
  finishedAt: Date;
}