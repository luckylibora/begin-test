import { IsDefined, IsString } from 'class-validator';

export class TimeRangeUpdateDto {
  @IsDefined()
  @IsString()
  name: string;
}