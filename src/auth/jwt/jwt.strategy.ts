import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { JwtConfigService } from './jwt.config.service';
import { JwtPayload } from './jwt.payload';
import { UserEntity } from '../../users/users/user.entity';
import { UsersService } from '../../users/users/users.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private jwtConfigService: JwtConfigService,
    private usersService: UsersService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConfigService.createJwtOptions().secret,
    });
  }

  async validate({ id }: JwtPayload): Promise<UserEntity> {
    return await this.usersService.findById(id);
  }
}