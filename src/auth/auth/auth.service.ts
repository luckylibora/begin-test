import { Injectable } from '@nestjs/common';
import { UsersService } from '../../users/users/users.service';
import { AuthLoginDto } from './dto/auth.login.dto';
import { UserEntity } from '../../users/users/user.entity';
import { HashService } from '../../common/hash/hash.service';
import { AuthTokenResponse } from './dto/auth.token.response';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from '../jwt/jwt.payload';

@Injectable()
export class AuthService {
  constructor(
    private hashService: HashService,
    private jwtService: JwtService,
    private usersService: UsersService,
  ) {
  }

  async checkLoginData({ email, password }: AuthLoginDto): Promise<UserEntity> {
    const user = await this.usersService.findOneByEmail(email);
    if (!user) {
      return null;
    }

    const isMatched = this.hashService.verifyStringHash(password, {
      hash: user.passwordHash,
      salt: user.passwordSalt,
    });
    if (!isMatched) {
      return null;
    }

    return user;
  }

  generateToken({ id }: UserEntity): AuthTokenResponse {
    const jwtPayload: JwtPayload = { id };
    return {
      token: this.jwtService.sign(jwtPayload),
    };
  }
}