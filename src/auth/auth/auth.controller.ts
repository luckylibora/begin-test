import { Controller, UseGuards, Post, Request } from '@nestjs/common';
import { LocalAuthGuard } from '../local/local.auth.guard';
import { AuthService } from './auth.service';
import { AuthTokenResponse } from './dto/auth.token.response';
import { ApiBody, ApiOkResponse } from '@nestjs/swagger';
import { AuthLoginDto } from './dto/auth.login.dto';

@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
  ) {
  }

  @UseGuards(LocalAuthGuard)
  @Post('login')
  @ApiBody({ type: AuthLoginDto })
  @ApiOkResponse({ type: AuthTokenResponse })
  async login(@Request() req): Promise<AuthTokenResponse> {
    return this.authService.generateToken(req.user);
  }
}