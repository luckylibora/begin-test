import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UserEntity } from '../../users/users/user.entity';
import { LocalStrategy } from '../local/local.strategy';

describe('AuthController', () => {
  let app: INestApplication;
  let authService: AuthService;

  const authServiceMock = {
    checkLoginData: jest.fn(),
    generateToken: jest.fn(),
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        AuthService,
        LocalStrategy,
      ],
      controllers: [AuthController],
    })
      .overrideProvider(AuthService)
      .useValue(authServiceMock)
      .compile();

    app = moduleRef.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
    authService = moduleRef.get(AuthService);
  });

  describe('POST /auth/login', () => {
    it('should return token', () => {
      const user = new UserEntity({
        id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
        email: 'test@test.com',
        passwordHash: '1q1w2e2ee2',
        passwordSalt: 'r3fgef32f232f',
      });
      jest.spyOn(authService, 'checkLoginData')
        .mockResolvedValue(user);
      jest.spyOn(authService, 'generateToken')
        .mockReturnValue({ token: '123456' });
      return request(app.getHttpServer())
        .post('/auth/login')
        .send({ email: 'test@test.com', password: '123123123' })
        .expect(201)
        .expect({ token: '123456' });
    });

    it('should return 401 for invalid credentials', () => {
      jest.spyOn(authService, 'checkLoginData')
        .mockResolvedValue(null);
      return request(app.getHttpServer())
        .post('/auth/login')
        .send({ email: 'test@test.com', password: '1231231234' })
        .expect(401);
    });
  });

  afterAll(async () => {
    await app.close();
  });
});