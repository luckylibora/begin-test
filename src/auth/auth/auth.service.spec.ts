import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { CommonModule } from '../../common/common.module';
import { UsersService } from '../../users/users/users.service';
import { UserEntity } from '../../users/users/user.entity';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { HashService } from '../../common/hash/hash.service';

describe('AuthService', () => {
  let authService: AuthService;

  const hashServiceMock = {
    verifyStringHash: jest.fn(),
  };
  const jwtServiceMock = {
    sign: jest.fn(),
  };
  const usersServiceMock = {
    findOneByEmail: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        UsersService,
      ],
      imports: [
        JwtModule,
        CommonModule,
      ],
    }).overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .overrideProvider(JwtService)
      .useValue(jwtServiceMock)
      .overrideProvider(HashService)
      .useValue(hashServiceMock)
      .compile();

    authService = module.get(AuthService);
  });

  describe('#checkLoginData', () => {
    it('should return user', async () => {
      const savedUser = new UserEntity({
        id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
        email: 'test@test.com',
        passwordHash: '1q1w2e2ee2',
        passwordSalt: 'r3fgef32f232f',
      });
      jest.spyOn(usersServiceMock, 'findOneByEmail')
        .mockResolvedValue(savedUser);
      jest.spyOn(hashServiceMock, 'verifyStringHash')
        .mockResolvedValue(true);
      const user = await authService.checkLoginData({ email: 'test@test.com', password: '123' });
      expect(user).toBe(savedUser);
    });

    it('should return null for non-existing user', async () => {
      jest.spyOn(usersServiceMock, 'findOneByEmail')
        .mockReturnValue(null);
      const user = await authService.checkLoginData({ email: 'test@test.com', password: '123' });
      expect(user).toBeNull();
    });

    it('should return null if password doesnt match', async () => {
      const savedUser = new UserEntity({
        id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
        email: 'test@test.com',
        passwordHash: '1q1w2e2ee2',
        passwordSalt: 'r3fgef32f232f',
      });
      jest.spyOn(usersServiceMock, 'findOneByEmail')
        .mockResolvedValue(savedUser);
      jest.spyOn(hashServiceMock, 'verifyStringHash')
        .mockReturnValue(false);
      const user = await authService.checkLoginData({ email: 'test@test.com', password: '123' });
      expect(user).toBeNull();
    });
  });

  describe('#generateToken', () => {
    it('should return token for user', () => {
      const user = new UserEntity({
        id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
        email: 'test@test.com',
        passwordHash: '1q1w2e2ee2',
        passwordSalt: 'r3fgef32f232f',
      });
      const spyOnJwtServiceSign = jest.spyOn(jwtServiceMock, 'sign')
        .mockReturnValue('123456');
      const { token } = authService.generateToken(user);
      expect(token).toBe('123456');
      expect(spyOnJwtServiceSign).toBeCalledWith({ id: 1 });
    });
  });
});