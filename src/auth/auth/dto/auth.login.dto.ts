import { IsEmail, MinLength } from 'class-validator';
import { userPasswordMinLength } from '../../../users/users/dto/user.validation.consts';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class AuthLoginDto {
  @IsEmail()
  @ApiModelProperty()
  email: string;

  @MinLength(userPasswordMinLength)
  @ApiModelProperty()
  password: string;
}