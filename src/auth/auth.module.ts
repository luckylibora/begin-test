import { Module } from '@nestjs/common';
import { UsersModule } from '../users/users.module';
import { AuthService } from './auth/auth.service';
import { LocalStrategy } from './local/local.strategy';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './auth/auth.controller';
import { JwtConfigService } from './jwt/jwt.config.service';
import { JwtStrategy } from './jwt/jwt.strategy';

@Module({
  imports: [
    JwtModule.registerAsync({
      useClass: JwtConfigService,
    }),
    UsersModule,
  ],
  providers: [
    AuthService,
    JwtConfigService,
    JwtStrategy,
    LocalStrategy,
  ],
  controllers: [
    AuthController,
  ],
})
export class AuthModule {
}