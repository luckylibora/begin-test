import { MigrationInterface, QueryRunner, Table, TableForeignKey, TableIndex } from 'typeorm';

export class TimeRangeTable1601653849050 implements MigrationInterface {
  name = 'TimeRangeTable1601653849050';

  async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: 'time_ranges',
        columns: [{
          name: 'id',
          type: 'integer',
          isPrimary: true,
          generationStrategy: 'increment',
          isGenerated: true,
        }, {
          name: 'createdAt',
          type: 'datetime',
          default: 'datetime(\'now\')',
          isNullable: false,
        }, {
          name: 'updatedAt',
          type: 'datetime',
          default: 'datetime(\'now\')',
          isNullable: false,
        }, {
          name: 'userId',
          type: 'varchar',
          isNullable: false,
        }, {
          name: 'name',
          type: 'varchar',
          isNullable: false,
        }, {
          name: 'startedAt',
          type: 'datetime',
          isNullable: true,
        }, {
          name: 'finishedAt',
          type: 'datetime',
          isNullable: true,
        }],
      }),
    );
    await queryRunner.createForeignKey('time_ranges', new TableForeignKey({
      name: 'fk_time_ranges_userId_users_id',
      columnNames: ['userId'],
      referencedTableName: 'users',
      referencedColumnNames: ['id'],
      onDelete: 'NO ACTION',
    }));
    await queryRunner.createIndex('time_ranges', new TableIndex({
      name: 'idx_time_ranges_userId_name',
      columnNames: ['name', 'userId'],
      isUnique: true,
    }));
    await queryRunner.createIndex('time_ranges', new TableIndex({
      name: 'idx_time_ranges_startedAt',
      columnNames: ['startedAt'],
    }));
  }

  async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('users');
  }
}