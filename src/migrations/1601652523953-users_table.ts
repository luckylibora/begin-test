import { MigrationInterface, QueryRunner, Table, TableIndex } from 'typeorm';

export class UsersTable1601652523953 implements MigrationInterface {
  name = 'UsersTable1601652523953';

  async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(
      new Table({
        name: 'users',
        columns: [{
          name: 'id',
          type: 'integer',
          isPrimary: true,
          generationStrategy: 'increment',
          isGenerated: true,
        }, {
          name: 'createdAt',
          type: 'datetime',
          default: 'datetime(\'now\')',
          isNullable: false,
        }, {
          name: 'updatedAt',
          type: 'datetime',
          default: 'datetime(\'now\')',
          isNullable: false,
        }, {
          name: 'email',
          type: 'varchar',
          isNullable: false,
        }, {
          name: 'passwordHash',
          type: 'varchar',
          isNullable: false,
        }, {
          name: 'passwordSalt',
          type: 'varchar',
          isNullable: false,
        }],
      }),
    );
    await queryRunner.createIndex('users', new TableIndex({
      name: 'idx_users_email',
      columnNames: ['email'],
      isUnique: true,
    }));
  }

  async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropTable('users');
  }
}