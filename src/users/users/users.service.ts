import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from './user.entity';
import { QueryFailedError, Repository } from 'typeorm';
import { UserCreateDto } from './dto/user.create.dto';
import { HashService } from '../../common/hash/hash.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity)
    private usersRepository: Repository<UserEntity>,
    private hashService: HashService,
  ) {
  }

  async create({ email, password }: UserCreateDto): Promise<UserEntity> {
    try {
      const { hash, salt } = this.hashService.makeHashWithSalt(password);
      const entity = new UserEntity({
        email,
        passwordHash: hash,
        passwordSalt: salt,
      });
      await this.usersRepository.insert(entity);
      return entity;
    } catch (err) {
      this.handleDbWriteError(err);
    }
  }

  async findById(id: number): Promise<UserEntity> {
    return await this.usersRepository.findOne(id);
  }

  async findOneByEmail(email: string): Promise<UserEntity> {
    return await this.usersRepository.findOne({
      where: { email },
    });
  }

  private handleDbWriteError(err: Error) {
    if (err instanceof QueryFailedError) {
      if (err.message === 'SQLITE_CONSTRAINT: UNIQUE constraint failed: users.email') {
        throw new BadRequestException(`User with this email is already registered`);
      }
    }
    throw err;
  }
}