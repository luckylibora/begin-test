import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UserEntity } from './user.entity';
import { QueryFailedError, Repository } from 'typeorm';
import { CommonModule } from '../../common/common.module';
import { BadRequestException } from '@nestjs/common';

describe('UsersService', () => {
  let usersService: UsersService;
  let repository: Repository<UserEntity>;

  const repositoryToken = getRepositoryToken(UserEntity);
  const usersRepositoryMock = {
    insert: jest.fn(),
    findOne: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: repositoryToken,
          useValue: usersRepositoryMock,
        },
      ],
      imports: [CommonModule],
    }).compile();

    usersService = module.get(UsersService);
    repository = module.get(repositoryToken);
  });

  describe('#create', () => {
    it('should create user with required properties', async () => {
      const mockedInsert = jest.spyOn(repository, 'insert');
      await usersService.create({ email: 'test@test.com', password: '123' });
      const insertArgs = mockedInsert.mock.calls[0];
      const userEntityArg = insertArgs[0];
      expect(userEntityArg).toHaveProperty('email', 'test@test.com');
      expect(userEntityArg).toHaveProperty('passwordHash');
      expect(userEntityArg).toHaveProperty('passwordSalt');
    });

    it('should throw BadRequestException if user with email is already created', async () => {
      const queryErr = new QueryFailedError(
        '',
        [],
        'SQLITE_CONSTRAINT: UNIQUE constraint failed: users.email',
      );
      jest.spyOn(repository, 'insert').mockRejectedValue(queryErr);
      const registerDto = { email: 'test@test.com', password: '123' };
      try {
        await usersService.create(registerDto);
        fail('exception is not thrown');
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
      }
    });

    it('should throw original error in other cases', async () => {
      const originalErr = new Error();
      jest.spyOn(repository, 'insert').mockRejectedValue(originalErr);
      const registerDto = { email: 'test@test.com', password: '123' };
      try {
        await usersService.create(registerDto);
        fail('exception is not thrown');
      } catch (err) {
        expect(err).toBe(originalErr);
      }
    });
  });

  describe('#findById', () => {
    it('should return one user', async () => {
      const savedUser = new UserEntity({
        id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
        email: 'test@test.com',
        passwordHash: '1q1w2e2ee2',
        passwordSalt: 'r3fgef32f232f',
      });
      jest.spyOn(repository, 'findOne').mockResolvedValue(savedUser);
      const user = await usersService.findById(1);
      expect(user).toBe(savedUser);
    });
  });

  describe('#findOneWithEmail', () => {
    it('should return one user', async () => {
      const savedUser = new UserEntity({
        id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
        email: 'test@test.com',
        passwordHash: '1q1w2e2ee2',
        passwordSalt: 'r3fgef32f232f',
      });
      jest.spyOn(repository, 'findOne').mockResolvedValue(savedUser);
      const user = await usersService.findOneByEmail('test@test.com');
      expect(user).toBe(savedUser);
    });
  });
});