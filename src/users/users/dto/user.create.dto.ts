import { IsEmail, MinLength } from 'class-validator';
import { userPasswordMinLength } from './user.validation.consts';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class UserCreateDto {
  @IsEmail()
  @ApiModelProperty()
  email: string;

  @MinLength(userPasswordMinLength)
  @ApiModelProperty()
  password: string;
}