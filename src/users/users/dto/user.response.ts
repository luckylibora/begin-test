import { BaseResponse } from '../../../common/dto/base.response';
import { Exclude, Expose } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

@Exclude()
export class UserResponse extends BaseResponse {
  @Expose()
  @ApiModelProperty()
  email: string;
}