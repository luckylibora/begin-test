import { Body, Controller, Post } from '@nestjs/common';
import { UserCreateDto } from './dto/user.create.dto';
import { UsersService } from './users.service';
import { plainToClass } from 'class-transformer';
import { UserResponse } from './dto/user.response';
import { ApiOkResponse } from '@nestjs/swagger';

@Controller('users')
export class UsersController {
  constructor(
    private usersService: UsersService,
  ) {
  }

  @Post()
  @ApiOkResponse({ type: UserResponse })
  async register(@Body() dto: UserCreateDto): Promise<UserResponse> {
    const entity = await this.usersService.create(dto);
    return plainToClass(UserResponse, entity);
  }
}
