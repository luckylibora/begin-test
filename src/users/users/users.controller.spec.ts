import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { UserEntity } from './user.entity';

describe('UsersController', () => {
  let app: INestApplication;
  let usersService: UsersService;

  const usersServiceMock = {
    create: jest.fn(),
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [UsersService],
      controllers: [UsersController],
    })
      .overrideProvider(UsersService)
      .useValue(usersServiceMock)
      .compile();

    app = moduleRef.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
    usersService = moduleRef.get(UsersService);
  });

  describe('POST /users', () => {
    it('should create new user', () => {
      const createdAt = new Date('2020-01-01');
      jest.spyOn(usersService, 'create')
        .mockResolvedValue(new UserEntity({
          id: 1,
          createdAt,
          updatedAt: createdAt,
          email: 'test@test.com',
          passwordHash: '123rfafa',
          passwordSalt: 'r23f32f',
        }));
      return request(app.getHttpServer())
        .post('/users')
        .send({ email: 'test@test.com', password: '123123123' })
        .expect(201)
        .expect({
          id: 1,
          createdAt: createdAt.toISOString(),
          updatedAt: createdAt.toISOString(),
          email: 'test@test.com',
        });
    });

    it('should return 400 for non email', () => {
      return request(app.getHttpServer())
        .post('/users')
        .send({ email: 'test', password: '123123123' })
        .expect(400);
    });

    it('should return 400 for short password', () => {
      return request(app.getHttpServer())
        .post('/users')
        .send({ email: 'test@test.com', password: '123' })
        .expect(400);
    });
  });

  afterAll(async () => {
    await app.close();
  });
});