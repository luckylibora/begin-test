import { Column, Entity, OneToMany } from 'typeorm';
import { TimeRangeEntity } from '../../time.tracker/time.range/time.range.entity';
import { BaseEntity } from '../../common/typeorm/base.entity';

@Entity({ name: 'users' })
export class UserEntity extends BaseEntity {
  @Column()
  email: string;

  @Column()
  passwordSalt: string;

  @Column()
  passwordHash: string;

  @OneToMany(
    () => TimeRangeEntity,
    (timeRange) => timeRange.user,
  )
  timeRanges: TimeRangeEntity[];

  constructor(user: Partial<UserEntity>) {
    super();
    Object.assign(this, user);
  }
}