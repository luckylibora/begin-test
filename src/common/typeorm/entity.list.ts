export interface EntityList<T> {
  entities: T[];
  count: number;
}