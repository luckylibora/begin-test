import { Expose } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export abstract class BaseResponse {
  @Expose()
  @ApiModelProperty()
  id: string;

  @Expose()
  @ApiModelProperty()
  createdAt: Date;

  @Expose()
  @ApiModelProperty()
  updatedAt: Date;
}