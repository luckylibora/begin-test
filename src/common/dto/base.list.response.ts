import { Type } from '@nestjs/common';
import { plainToClass } from 'class-transformer';

export class BaseListResponse<T, Response> {
  records: Response[];

  constructor(responseClass: Type<Response>, records: T[], public count: number) {
    this.records = records.map((record) => plainToClass(responseClass, record));
  }
}