import { IsInt } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class IdParamDto {
  @IsInt()
  @Type(() => Number)
  @ApiModelProperty()
  id: number;
}