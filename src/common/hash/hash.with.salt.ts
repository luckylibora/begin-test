export interface HashWithSalt {
  hash: string;
  salt: string;
}