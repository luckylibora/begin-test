import { Test, TestingModule } from '@nestjs/testing';
import { HashService } from './hash.service';

describe('HashService', () => {
  let hashService: HashService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HashService],
    }).compile();

    hashService = module.get(HashService);
  });

  it('should successfully verify hash with salt', () => {
    const s = '123';
    const { hash, salt } = hashService.makeHashWithSalt(s);
    const isValid = hashService.verifyStringHash(s, { hash, salt });
    expect(isValid).toBe(true);
  });

  it('should return false for different strings', () => {
    const validString = '123';
    const { hash, salt } = hashService.makeHashWithSalt(validString);
    const invalidString = '1234'
    const isValid = hashService.verifyStringHash(invalidString, { hash, salt });
    expect(isValid).toBe(false);
  });
});