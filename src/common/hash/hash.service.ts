import { Injectable } from '@nestjs/common';
import { HashWithSalt } from './hash.with.salt';
import * as crypto from 'crypto';

@Injectable()
export class HashService {
  generateSalt(length = 32): string {
    return crypto.randomBytes(length).toString('base64');
  }

  getHash(s: string, salt: string): string {
    return crypto.createHash('sha256')
      .update(salt + s)
      .digest('base64');
  }

  makeHashWithSalt(s: string): HashWithSalt {
    const salt = this.generateSalt();
    const hash = this.getHash(s, salt);
    return { hash, salt };
  }

  verifyStringHash(s: string, { hash, salt }: HashWithSalt): boolean {
    return this.getHash(s, salt) === hash;
  }
}