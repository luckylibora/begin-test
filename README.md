## Description

Homework

## Installation

```bash
$ npm ci
```

## Running migrations
```bash
$ npm run migrations:run 
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# run tests
$ npm run test

# test coverage
$ npm run test:cov
```

### Swagger

Swagger UI is available on `/swagger`, swagger json is available on `/swagger-json`